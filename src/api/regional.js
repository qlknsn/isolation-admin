import http from '@/api/interceptor'

export const putUnBinding=(params)=>{//更新账户
    return http.put(`/adminapi/device/unBinding?id=`+params).then(res=>res.data)
}

export const alarmDetail = params => {//历史记录
    return http.get("/adminapi/alarm/alarmDetail",{params}).then(res => res.data);
};

export const areaAdd = params => {//区域管理，添加
    return http.post("/adminapi/area/add",{},{params}).then(res => res.data);
};