const axios = require('axios'); 


/**
 * @title 报警信息分页（报警信息）
 * @path /alarm
 * 
 */
exports.get_alarm = function(params, options = {}){
  let interfaceData={
  "title": "报警信息分页（报警信息）",
  "path": "/alarm",
  "method": "GET",
  "req_params": [],
  "req_query": [
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e6881",
      "name": "startTime",
      "example": "2020-02-09 15:00",
      "desc": "开始时间"
    },
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e6880",
      "name": "endTime",
      "example": "2020-02-09 15:00",
      "desc": "结束时间"
    },
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e687f",
      "name": "type",
      "example": "",
      "desc": "开门，拆除，低电量"
    },
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e687e",
      "name": "deviceId",
      "example": "",
      "desc": "设备编号"
    },
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e687d",
      "name": "areaId",
      "example": "",
      "desc": "区域"
    },
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e687c",
      "name": "start",
      "example": "1",
      "desc": "分页"
    },
    {
      "required": "0",
      "_id": "5e538d8e706fec00876e687b",
      "name": "count",
      "example": "10",
      "desc": "分页"
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 数据监控（数据统计）
 * @path /device/dataMonitor/{unit}
 * 
 */
exports.get_device_dataMonitor_unit = function(params, options = {}){
  let interfaceData={
  "title": "数据监控（数据统计）",
  "path": "/device/dataMonitor/{unit}",
  "method": "GET",
  "req_params": [
    {
      "_id": "5e522eb9706fec00876e6858",
      "name": "unit",
      "example": "day/hour",
      "desc": "hour/day，按24小时/按7天"
    }
  ],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 设备列表
 * @path /device/{pageNum}/{pageSize}
 * 
 */
exports.post_device_pageNum_pageSize = function(params, options = {}){
  let interfaceData={
  "title": "设备列表",
  "path": "/device/{pageNum}/{pageSize}",
  "method": "POST",
  "req_params": [
    {
      "_id": "5e563b3c706fec00876e6891",
      "name": "pageNum",
      "example": "1",
      "desc": "当前页"
    },
    {
      "_id": "5e563b3c706fec00876e6890",
      "name": "pageSize",
      "example": "10",
      "desc": "每页数量"
    }
  ],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e563b3c706fec00876e6892",
      "name": "Content-Type",
      "value": "application/json"
    }
  ],
  "req_body_type": "json",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
      "model": {
        "type": "string",
        "description": "型号"
      },
      "areaId": {
        "type": "number",
        "description": "区域id"
      },
      "status": {
        "type": "string",
        "description": "设备状态  0：初始状态 1：已安装   2：已拆卸"
      },
      "responsiblePerson": {
        "type": "string",
        "description": "责任人姓名"
      },
      "responsibleOrganization": {
        "type": "string",
        "description": "责任人单位"
      },
      "isolateName": {
        "type": "string",
        "description": "隔离人姓名"
      },
      "isolateNumber": {
        "type": "string",
        "description": "隔离人电话"
      },
      "serialNo": {
        "type": "string",
        "description": "行政区划编码"
      },
      "areaDetail": {
        "type": "string",
        "description": "安装位置"
      },
      "createTime": {
        "type": "string",
        "description": "开始时间"
      },
      "endTime": {
        "type": "string",
        "description": "结束时间"
      }
    },
    "required": [
      "createTime",
      "endTime"
    ]
  }
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 区域列表展示
 * @path /area
 * 
 */
exports.get_area = function(params, options = {}){
  let interfaceData={
  "title": "区域列表展示",
  "path": "/area",
  "method": "GET",
  "req_params": [],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e429a9a706fec00876e65b6",
      "name": "Cookie",
      "value": "guardian=2db22c72-8dc3-4de4-b256-cb9d3afde02c",
      "desc": "调用接口携带cookie值"
    }
  ],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 用户登录
 * @path /user/login
 * 
 */
exports.post_user_login = function(params, options = {}){
  let interfaceData={
  "title": "用户登录",
  "path": "/user/login",
  "method": "POST",
  "req_params": [],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e538d34706fec00876e6873",
      "name": "Content-Type",
      "value": "application/json"
    }
  ],
  "req_body_type": "json",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
      "username": {
        "type": "string",
        "description": "用户名"
      },
      "password": {
        "type": "string",
        "description": "密码"
      }
    },
    "required": [
      "username",
      "password"
    ]
  }
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 用户注销
 * @path /user/loginOut
 * 
 */
exports.post_user_loginOut = function(params, options = {}){
  let interfaceData={
  "title": "用户注销",
  "path": "/user/loginOut",
  "method": "POST",
  "req_params": [],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e477a89706fec00876e67ae",
      "name": "Content-Type",
      "value": "application/x-www-form-urlencoded"
    }
  ],
  "req_body_type": "form",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 设备信息统计
 * @path /device/statistic
 * 
 */
exports.get_device_statistic = function(params, options = {}){
  let interfaceData={
  "title": "设备信息统计",
  "path": "/device/statistic",
  "method": "GET",
  "req_params": [],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 数据监控（数据统计）_copy
 * @path /dataMonitor/{unit}_1581662848915
 * 
 */
exports.get_dataMonitor_unit_1581662848915 = function(params, options = {}){
  let interfaceData={
  "title": "数据监控（数据统计）_copy",
  "path": "/dataMonitor/{unit}_1581662848915",
  "method": "GET",
  "req_params": [
    {
      "_id": "5e538fea706fec00876e6883",
      "name": "unit",
      "example": "day/hour",
      "desc": "hour/day，按24小时/按7天"
    }
  ],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 数据监控（数据统计)(大屏)
 * @path /datav/dataMonitor/{unit}
 * 
 */
exports.get_datav_dataMonitor_unit = function(params, options = {}){
  let interfaceData={
  "title": "数据监控（数据统计)(大屏)",
  "path": "/datav/dataMonitor/{unit}",
  "method": "GET",
  "req_params": [
    {
      "_id": "5e47f42b706fec00876e67cb",
      "name": "unit",
      "example": "day/hour",
      "desc": "hour/day，按24小时/按7天"
    }
  ],
  "req_query": [
    {
      "required": "1",
      "_id": "5e47f42b706fec00876e67cc",
      "name": "auth_token",
      "example": "-422671539",
      "desc": "auth_token"
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 设备信息统计（大屏）
 * @path /datav/statistic
 * 
 */
exports.get_datav_statistic = function(params, options = {}){
  let interfaceData={
  "title": "设备信息统计（大屏）",
  "path": "/datav/statistic",
  "method": "GET",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e47f474706fec00876e67d5",
      "name": "auth_token",
      "example": "-422671539",
      "desc": ""
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 报警信息分页（报警信息）(大屏)
 * @path /datav/alarm
 * 
 */
exports.get_datav_alarm = function(params, options = {}){
  let interfaceData={
  "title": "报警信息分页（报警信息）(大屏)",
  "path": "/datav/alarm",
  "method": "GET",
  "req_params": [],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 大屏权限认证
 * @path /datav/auth
 * 
 */
exports.get_datav_auth = function(params, options = {}){
  let interfaceData={
  "title": "大屏权限认证",
  "path": "/datav/auth",
  "method": "GET",
  "req_params": [],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 获取大屏统计的区域地理位置
 * @path /datav/geoLocation
 * 
 */
exports.get_datav_geoLocation = function(params, options = {}){
  let interfaceData={
  "title": "获取大屏统计的区域地理位置",
  "path": "/datav/geoLocation",
  "method": "GET",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e48cdd4706fec00876e67fc",
      "name": "auth_token",
      "example": "-1914691464",
      "desc": ""
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 获取大屏统计得区域名称
 * @path /datav/userInfo
 * 
 */
exports.get_datav_userInfo = function(params, options = {}){
  let interfaceData={
  "title": "获取大屏统计得区域名称",
  "path": "/datav/userInfo",
  "method": "GET",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e48cde3706fec00876e67fd",
      "name": "auth_token",
      "example": "-1914691464",
      "desc": ""
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 地图模式获取所有的设备列表
 * @path /device/all
 * 
 */
exports.get_device_all = function(params, options = {}){
  let interfaceData={
  "title": "地图模式获取所有的设备列表",
  "path": "/device/all",
  "method": "GET",
  "req_params": [],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 修改密码
 * @path /user/updatePwd
 * 
 */
exports.put_user_updatePwd = function(params, options = {}){
  let interfaceData={
  "title": "修改密码",
  "path": "/user/updatePwd",
  "method": "PUT",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e4e55a9706fec00876e682c",
      "name": "password",
      "example": "admin123456",
      "desc": "新密码"
    }
  ],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e4e55a9706fec00876e682d",
      "name": "Content-Type",
      "value": "application/x-www-form-urlencoded"
    }
  ],
  "req_body_type": "form",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 责任人模糊查询
 * @path /user/userList
 * 
 */
exports.get_user_userList = function(params, options = {}){
  let interfaceData={
  "title": "责任人模糊查询",
  "path": "/user/userList",
  "method": "GET",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e4f31f9706fec00876e682e",
      "name": "name",
      "example": "张",
      "desc": "责任人姓名模糊匹配"
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 修改设备信息
 * @path /device
 * 
 */
exports.put_device = function(params, options = {}){
  let interfaceData={
  "title": "修改设备信息",
  "path": "/device",
  "method": "PUT",
  "req_params": [],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e51e5f3706fec00876e6838",
      "name": "Content-Type",
      "value": "application/json"
    }
  ],
  "req_body_type": "json",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
      "id": {
        "type": "string",
        "description": "设备id"
      },
      "isolateName": {
        "type": "string",
        "description": "隔离人姓名"
      },
      "responsibleUserId": {
        "type": "number",
        "description": "负责人userId"
      },
      "isolateNumber": {
        "type": "string",
        "description": "隔离人电话"
      },
      "organization": {
        "type": "string",
        "description": "负责人单位"
      }
    }
  }
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 自我隔离分页列表
 * @path /self/{pageNum}/{pageSize}
 * 
 */
exports.post_self_pageNum_pageSize = function(params, options = {}){
  let interfaceData={
  "title": "自我隔离分页列表",
  "path": "/self/{pageNum}/{pageSize}",
  "method": "POST",
  "req_params": [
    {
      "_id": "5e563ef8706fec00876e6897",
      "name": "pageNum",
      "example": "1",
      "desc": "当前页"
    },
    {
      "_id": "5e563ef8706fec00876e6896",
      "name": "pageSize",
      "example": "10",
      "desc": "每页数量"
    }
  ],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e563ef8706fec00876e6898",
      "name": "Content-Type",
      "value": "application/json"
    }
  ],
  "req_body_type": "json",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
      "name": {
        "type": "string",
        "description": "隔离人姓名"
      },
      "addressType": {
        "type": "string",
        "description": "地址类型"
      },
      "community": {
        "type": "string",
        "description": "社区模糊查询"
      },
      "areaId": {
        "type": "number",
        "description": "区域id"
      },
      "fever": {
        "type": "string",
        "description": "是否发热 0 未发热  1发热"
      },
      "responsiblePerson": {
        "type": "string",
        "description": "责任人名字 模糊查询"
      },
      "provenance": {
        "type": "string",
        "description": "返沪出发地"
      },
      "status": {
        "type": "number",
        "description": "0,未结束，1：已结束"
      },
      "createTime": {
        "type": "string",
        "description": "开始时间"
      },
      "endTime": {
        "type": "string",
        "description": "结束时间"
      }
    },
    "required": [
      "createTime",
      "endTime"
    ]
  }
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 更新自我隔离信息
 * @path /self
 * 
 */
exports.put_self = function(params, options = {}){
  let interfaceData={
  "title": "更新自我隔离信息",
  "path": "/self",
  "method": "PUT",
  "req_params": [],
  "req_query": [],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e538dc1706fec00876e6882",
      "name": "Content-Type",
      "value": "application/json"
    }
  ],
  "req_body_type": "json",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": {
    "$schema": "http://json-schema.org/draft-04/schema#",
    "type": "object",
    "properties": {
      "id": {
        "type": "number"
      },
      "name": {
        "type": "string",
        "description": "隔离人名字"
      },
      "sex": {
        "type": "string",
        "description": "性别 1：男 2女"
      },
      "address": {
        "type": "string",
        "description": "在沪地址"
      },
      "coordinate": {
        "type": "string",
        "description": "在沪经纬度"
      },
      "addressType": {
        "type": "string",
        "description": "地址类型"
      },
      "community": {
        "type": "string",
        "description": "社区"
      },
      "areaId": {
        "type": "number",
        "description": "区域id"
      },
      "households": {
        "type": "number",
        "description": "户数"
      },
      "peopleCounts": {
        "type": "number",
        "description": "人数"
      },
      "phone": {
        "type": "string",
        "description": "电话"
      },
      "idCard": {
        "type": "string",
        "description": "身份证"
      },
      "fever": {
        "type": "number",
        "description": "是否发热:0 未发热，1：发热"
      },
      "wayOfReturn": {
        "type": "string",
        "description": "返沪交通工具"
      },
      "dateOfReturn": {
        "type": "string",
        "description": "返沪时间"
      },
      "condition": {
        "type": "string",
        "description": "是否有居家隔离观察条件：0:没有，1有"
      },
      "startTime": {
        "type": "string",
        "description": "开始 隔离时间"
      },
      "endTime": {
        "type": "string",
        "description": "结束时间"
      },
      "responsibleUserId": {
        "type": "number",
        "description": "责任人id"
      },
      "provenance": {
        "type": "string",
        "description": "返沪出发地"
      },
      "remarks": {
        "type": "string"
      },
      "status": {
        "type": "number",
        "description": "0,未结束，1：已结束"
      }
    }
  }
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 查询所有自我隔离列表
 * @path /self
 * 
 */
exports.get_self = function(params, options = {}){
  let interfaceData={
  "title": "查询所有自我隔离列表",
  "path": "/self",
  "method": "GET",
  "req_params": [],
  "req_query": [],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 关注列表
 * @path /device/attention
 * 
 */
exports.get_device_attention = function(params, options = {}){
  let interfaceData={
  "title": "关注列表",
  "path": "/device/attention",
  "method": "GET",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e56326a706fec00876e688c",
      "name": "deviceId",
      "desc": "设备id"
    },
    {
      "required": "0",
      "_id": "5e56326a706fec00876e688b",
      "name": "status",
      "example": "",
      "desc": "0:已绑定，1：已删除"
    }
  ],
  "req_headers": [],
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

/**
 * @title 取消关注
 * @path /device/unAttention
 * 
 */
exports.put_device_unAttention = function(params, options = {}){
  let interfaceData={
  "title": "取消关注",
  "path": "/device/unAttention",
  "method": "PUT",
  "req_params": [],
  "req_query": [
    {
      "required": "1",
      "_id": "5e572ed3706fec00876e689b",
      "name": "id",
      "desc": "关注id"
    }
  ],
  "req_headers": [
    {
      "required": "1",
      "_id": "5e572ed3706fec00876e689c",
      "name": "Content-Type",
      "value": "application/x-www-form-urlencoded"
    }
  ],
  "req_body_type": "form",
  "req_body_is_json_schema": true,
  "req_body_form": [],
  "req_body_other": null
};
  return httpRequest(interfaceData,params, options)
}
  

const Ajv = require('ajv');

function  schemaValidator(schema, params) {
  try {
    const ajv = new Ajv({
      format: false,
      meta: false,
      schemaId: 'id'
    });
    let metaSchema = require('ajv/lib/refs/json-schema-draft-04.json');
    ajv.addMetaSchema(metaSchema);
    ajv._opts.defaultMeta = metaSchema.id;
    ajv._refs['http://json-schema.org/schema'] = 'http://json-schema.org/draft-04/schema';
    schema = schema || {
      type: 'object',
      title: 'empty object',
      properties: {}
    };
    const validate = ajv.compile(schema);
    let valid = validate(params);

    let message = '';
    if (!valid) {
      message += ajv.errorsText(validate.errors, { separator: '\n' });
    }

    return {
      valid: valid,
      message: message
    };
  } catch (e) {
    return {
      valid: false,
      message: e.message
    };
  }
};

function checkRequestParams (interfaceData, params) {
  const HTTP_METHOD = {
    GET: {
      request_body: false,
      default_tab: 'query',
    },
    POST: {
      request_body: true,
      default_tab: 'body',
    },
    PUT: {
      request_body: true,
      default_tab: 'body',
    },
    DELETE: {
      request_body: true,
      default_tab: 'body',
    },
    HEAD: {
      request_body: false,
      default_tab: 'query',
    },
    OPTIONS: {
      request_body: false,
      default_tab: 'query',
    },
    PATCH: {
      request_body: true,
      default_tab: 'body',
    },
  };

  const noRequiredArr = [];
  let validResult;

  for (let i = 0, l = interfaceData.req_query.length; i < l; i++) {
    let curQuery = interfaceData.req_query[i];
    if (curQuery && typeof curQuery === 'object' && curQuery.required === '1') {
      if (!params[curQuery.name]) {
        noRequiredArr.push (curQuery.name);
      }
    }
  }

  if (HTTP_METHOD[interfaceData.method].request_body) {
    // form 表单判断

    if (interfaceData.req_body_type === 'form') {
      let isFile = false;
      for (let y = 0; y < interfaceData.req_body_form.length; y++) {
        if (interfaceData.req_body_form[y].type === 'file') {
          isFile = true;
          break;
        }
      }
      if (!isFile) {
        for (
          (j = 0), (len = interfaceData.req_body_form.length);
          j < len;
          j++
        ) {
          let curForm = interfaceData.req_body_form[j];
          if (
            curForm &&
            typeof curForm === 'object' &&
            curForm.required === '1'
          ) {
            if (params[curForm.name]) {
              continue;
            }

            noRequiredArr.push (curForm.name);
          }
        }
      }
    }

    // json schema 判断
    if (
      interfaceData.req_body_type === 'json' &&
      interfaceData.req_body_is_json_schema === true
    ) {
      const schema = interfaceData.req_body_other;
      validResult = schemaValidator (schema, params);
    }
  }

  if (noRequiredArr.length > 0 || (validResult && !validResult.valid)) {
    let message = `错误信息：`;
    message += noRequiredArr.length > 0
      ? `缺少必须字段 ${noRequiredArr.join (',')}  `
      : '';
    message += validResult && !validResult.valid
      ? `shema 验证请求参数 ${validResult.message}`
      : '';

    throw new Error (message);
  }
}

  
function httpRequest(interfaceData, params, options){
  let  url=  interfaceData.path;
  let method = interfaceData.method;

  let isRestful = false;
  if(url.indexOf(':') > 0){
    isRestful = true;
  }else if(url.indexOf('{') > 0 && url.indexOf('}') > 0){
    isRestful = true;
  }

  if(isRestful){
    interfaceData.req_params.forEach(item=>{
      let val = params[item.name];
      if(!val){
        throw new Error('路径参数 ' + item.name + ' 不能为空')
      }
      url = url.replace(":" + item.name , val );
      url = url.replace("{" + item.name + "}", val );
      delete params[item.name]
    })
  }
function request(baseinfo, options = {}){
            let params = baseinfo.params;
            
            
            options = Object.assign({}, {
              url:"/adminapi"+baseinfo.url,
              method: baseinfo.method,
              data: params
            }, options)
  
    
            return axios(options)
          } 
  return request({
    url,
    method, 
    params
  }, options)
  
}
  