import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
        path: '/login',
        component: () =>
            import ('@/views/login/index'),
        hidden: true
    },

    {
        path: '/404',
        component: () =>
            import ('@/views/404'),
        hidden: true
    },

    {
        path: '/',
        component: Layout,
        show:'dataV:read',
        redirect: '/dashboard',
       
        meta: { title: '首页', icon: 'dashboard' },
        children: [{
            path: 'dashboard',
            name: 'Dashboard',
            component: () =>
                import ('@/views/dashboard/index'),
            meta: { title: '首页', icon: 'shouye' }
        }]
    },

    {
        path: '/dataScreen',
        component: Layout,
        show:'dataV:read',
        children: [{
            path: 'dataScreen',
            name: 'DataScreen',
            component: () =>
                import ('@/views/dataScreen/index'),
            meta: { title: '数据大屏', icon: 'daping' }
        }]
    },
    {
        path: '/self',
        component: Layout,
        show:'self-quarantine:read',
        children: [{
            path: 'self',
            name: 'Self',
            component: () =>
                import ('@/views/self/index'),
            meta: { title: '自我隔离', icon: 'ziwogeli' }
        }]
    },
    {
        path: '/distribution',
        component: Layout,
        redirect: '/distribution/table',
        name: 'Distribution',
        meta: { title: '超级管理', icon: 'guanli' },
        children: [
            // {
            //     path: 'distribution',
            //     name: 'Distribution',
            //     component: () =>
            //         import ('@/views/distribution/index'),
            //     meta: { title: '设备分配', icon: 'shebeifenpei' }
            // },
            {
                path: 'equipment',
                name: 'Equipment',
                show:'area:read',
                component: () =>
                    import ('@/views/equipment/index'),
                meta: { title: '区域管理', icon: 'quyuguanli' } //quyuguanli
            },
            {
                path: '/editpassword',
                name: 'Editpassword',
                show:'user:reset',
                component: () =>
                    import ('@/views/editPassword'),
                meta: { title: '修改密码', icon: 'edit' }
            },
            {
                path: '/account',
                name: 'account',
                show:'user:read',
                component: () =>import ('@/views/account/index'),
                meta: { title: '账号管理', icon: 'example' }
            },
            {
                path: '/person',
                name: 'account',
                show:'role:read',
                component: () =>import ('@/views/person/index'),
                meta: { title: '角色管理', icon: 'user' }
            },
        ]
    },
    {
        path: '/example',
        component: Layout,
        redirect: '/example/table',
        name: 'Example',
        meta: { title: '我的设备', icon: 'shebei' },
        children: [{
                path: 'table',
                name: 'Table',
                show:'map:read',
                component: () =>
                    import ('@/views/table/index'),
                meta: { title: '地图模式', icon: 'ditu' }
            },
            // {
            //     path: 'tree',
            //     name: 'Tree',
            //     component: () =>
            //         import ('@/views/tree/index'),
            //     meta: { title: '数据统计', icon: 'shujutongji' }
            // },
            {
                path: 'tree',
                name: 'Tree',
                show:'statistic:read',
                component: () =>
                    import ('@/views/dashboard/index'),
                meta: { title: '数据统计', icon: 'shujutongji' }
            },
            {
                path: 'alarm',
                name: 'Alarm',
                show:'alarm:read',
                component: () =>
                    import ('@/views/alarm/index'),
                meta: { title: '报警记录', icon: 'baojingjilu' }
            },
            {
                path: 'regional',
                name: 'Regional',
                show:'device:read',
                component: () =>
                    import ('@/views/regional/index'),
                meta: { title: '设备列表', icon: 'shebeilibiao' }
            },
        ]
    },

    

    // {
    //     path: '/nested',
    //     component: Layout,
    //     redirect: '/nested/menu1',
    //     name: 'Nested',
    //     meta: {
    //         title: 'Nested',
    //         icon: 'nested'
    //     },
    //     children: [{
    //             path: 'menu1',
    //             component: () =>
    //                 import ('@/views/nested/menu1/index'), // Parent router-view
    //             name: 'Menu1',
    //             meta: { title: 'Menu1' },
    //             children: [{
    //                     path: 'menu1-1',
    //                     component: () =>
    //                         import ('@/views/nested/menu1/menu1-1'),
    //                     name: 'Menu1-1',
    //                     meta: { title: 'Menu1-1' }
    //                 },
    //                 {
    //                     path: 'menu1-2',
    //                     component: () =>
    //                         import ('@/views/nested/menu1/menu1-2'),
    //                     name: 'Menu1-2',
    //                     meta: { title: 'Menu1-2' },
    //                     children: [{
    //                             path: 'menu1-2-1',
    //                             component: () =>
    //                                 import ('@/views/nested/menu1/menu1-2/menu1-2-1'),
    //                             name: 'Menu1-2-1',
    //                             meta: { title: 'Menu1-2-1' }
    //                         },
    //                         {
    //                             path: 'menu1-2-2',
    //                             component: () =>
    //                                 import ('@/views/nested/menu1/menu1-2/menu1-2-2'),
    //                             name: 'Menu1-2-2',
    //                             meta: { title: 'Menu1-2-2' }
    //                         }
    //                     ]
    //                 },
    //                 {
    //                     path: 'menu1-3',
    //                     component: () =>
    //                         import ('@/views/nested/menu1/menu1-3'),
    //                     name: 'Menu1-3',
    //                     meta: { title: 'Menu1-3' }
    //                 }
    //             ]
    //         },
    //         {
    //             path: 'menu2',
    //             component: () =>
    //                 import ('@/views/nested/menu2/index'),
    //             meta: { title: 'menu2' }
    //         }
    //     ]
    // },



    // 404 page must be placed at the end !!!
    { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router