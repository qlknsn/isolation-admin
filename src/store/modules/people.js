import {getRoleList, addRole,updateRole,deleteRole} from '../people/people'
import http from '@/api/interceptor'
import {ROLE_LIST,ROLE_UPDATE,ROLE_ADD, ROLE_DELETE} from '@/store/apiName'


const actions = {
    [ROLE_LIST]({commit},params){
        const {} = params;
        return getRoleList(params,{}).then(res=>res.data);
    },

    [ROLE_UPDATE]({commit},params){
        const {} = params;
        return updateRole(params,).then(res=>res.data);
    },

    [ROLE_ADD]({commit},params){
        const {name,permissions,remark} = params;
        return addRole({name,permissions,remark}).then(res=>res.data);
    },

    [ROLE_DELETE]({commit},params){
        const {id} = params;
        return deleteRole({id}).then(res=>res.data);
    }

    


}

export default{
    actions
}