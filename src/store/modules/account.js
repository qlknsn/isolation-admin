// import {Account} from '../account/apiurl'
import http from '@/api/interceptor'
import {ACCOUNT_LIST,ADD_ACCOUNT,SEARCH_ROLE} from '@/store/apiName'
import {
    getAccountList,
    addAccount,
    searchArea,
    searchRole,
    deleteAccount,
    singleAccount,
    updateAccount
} from '../account/account'

const state={
    roleList:[]
}

const mutations={
    // [ACCOUNT_LIST](state,data){//处理账号列表
    //     console.log(data)
    // },
    [SEARCH_ROLE](state,data){
        console.log(data)
        state.roleList = data
    }
}

const actions ={
    [ACCOUNT_LIST]({commit},params){//获取账号列表
        const {serialNo,username,roleId} =params
        // getAccountList(params).then(res=>commit('ACCOUNT_LIST',res.data))
       return getAccountList(params,{serialNo,username,roleId}).then(res=>res.data)
    },

    [ADD_ACCOUNT]({commit},params){//添加账户
       return addAccount(params).then(res=>res.data)
    },
     
    SEARCH_AREA({commit},params){
        return searchArea(params).then(res=>res.data)
    },

    SEARCH_ROLE({commit},params){//查询角色
        searchRole(params).then(res=>commit('SEARCH_ROLE',res.data))
    },

    DELETE_ACCOUNT({commit},params){//查询角色
        return deleteAccount(params).then(res=>res.data)
    },

    SINGLE_ACCOUNT({commit},params){
        return singleAccount(params).then(res=>res.data)
    },

    UPDATE_ACCOUNT({commit},params){
        return updateAccount(params).then(res=>{
            console.log(res)
            return res.data
        })
    }
    
}

export default{
    state,
    mutations,
    actions
}