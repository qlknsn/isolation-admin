import {People} from './apiurl'
import http from '@/api/interceptor'

export const getRoleList = (params,searchParams)=>{
    return http.post( `${People.role_list}/${params.pageNo}/${params.pageSize}`);
}

export const updateRole = (params)=>{
    return http.put(`${People.role_update}`,params);
}

export const addRole = (params)=>{
    return http.post(`${People.role_add}`,params);
}

export const deleteRole = (params)=>{
    return http.delete(`${People.role_delete}/${params.id}`)
}