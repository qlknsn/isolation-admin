import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import account from './modules/account'
import people from './modules/people'
Vue.use(Vuex)

const store = new Vuex.Store({
  state:{
    permission:'kong'
  },
  modules: {
    app,
    settings,
    user,
    account,
    people
  },
  getters
})

export default store
