import {Account} from './apiurl'
import http from '@/api/interceptor'
import qs from 'qs'
 
export const getAccountList=(params,searchParams)=>{//获取账户列表
    return http.post(`${Account.account_list}/page/${params.pageNo}/${params.pageSize}`,searchParams)
}

export const addAccount=(params)=>{//添加
    return http.post(`${Account.account_list}`,params)
}

export const searchArea=(params)=>{//查询区域
    return http.post(`${Account.area_list}`,qs.stringify(params),{headers:{'Content-Type':'application/x-www-form-urlencoded'}})
}

export const searchRole=()=>{//查询角色
    return http.get(`${Account.role_list}`)
}

export const deleteAccount=(params)=>{//查询角色
    return http.delete(`${Account.account_list}/${params.id}`)
}

export const singleAccount=(params)=>{//查询角色
    return http.get(`${Account.account_list}/${params.id}`)
}

export const updateAccount=(params)=>{//更新账户
    return http.put(`${Account.account_list}`,params)
}