/*
 * @Author: your name
 * @Date: 2020-02-11 12:21:42
 * @LastEditTime : 2020-02-13 14:50:21
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \isolation-admin\babel.config.js
 */
module.exports = {
  presets: [
    '@vue/app'
  ],
  plugins: [
    [
      '@babel/plugin-transform-modules-commonjs',
      {
        allowTopLevelThis: true,
      },
    ],
  ]
}
