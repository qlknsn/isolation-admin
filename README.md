<!--
 * @Author: your name
 * @Date: 2020-02-10 11:51:54
 * @LastEditTime : 2020-02-11 09:28:46
 * @LastEditors  : Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \vue-admin-template\README.md
 -->
#  isolation-admin

测试环境：http://192.168.63.200:89/isolation/isolation-admin/
生产环境：http://oyster.bearhunting.cn/isolation/isolation-admin/

## Build Setup

```bash
# 克隆项目
git clone http://gitlab.bearhunting.cn/frontend/isolation-admin.git

# 进入项目目录
cd isolation-admin

# 安装依赖
npm install

# 建议不要直接使用 cnpm 安装以来，会有各种诡异的 bug。可以通过如下操作解决 npm 下载速度慢的问题
npm install --registry=https://registry.npm.taobao.org

# 启动服务
npm run dev
```

浏览器访问 [http://localhost:9528](http://localhost:9528)

# 发布

## 测试环境
git push 到master分支

## 生产环境
git push 到deploy分支


# 接口代码生成器

## yapi-gen-js-code


### 环境
* node > 8.0
* yapi >= 1.5.2

根据 YApi 的接口定义生成 javascript 的请求函数，目前内置了 axios 请求模板

### 功能
1. 支持 restful api
2. 支持 get, post 等多种请求体
3. 支持验证请求参数，需要依赖 ava 

### 安装

```
npm i -g yapi-gen-js-code

```

### 使用

1.首先需要创建 `yapi-gen.config.js` 配置文件，如下所示：

```js
module.exports = {
  server: 'http://127.0.0.1:3000',
  token: '1f048e410bc22208297dec1113136cda58306d28fb0fa819652e659b16764be6',
  categoryId: 323
}

```

2.在当前目录下执行 yapi-gen-js-code

```
yapi-gen-js-code
```

执行完成后，即可生成 yapi-gen-js-code.js 请求文件


### 生成代码示例
/src/api/gen-api.js

### yapi-gen.config.js配置项说明

| name | 类型 | 默认值 | 描述信息 |  
| ---- | --- | --- | ---- | 
| server | String | - | 服务器地址，比如: http://192.168.63.200:3000 | 
| token | String | - | 项目token |  
| dist | String | - | 生成文件路径 |  
| template | String Or Function | axios | 模板名，目前仅内置了 axios 模板，自定义请查看下面文档 | 
| globalCode | String |const axios = require('axios'); | 全局代码，会注入到最前面 |  
| methodName | Function | 请参考源码 | 方法名生成函数，一般无需改动 |
| categoryId | String | - | 项目分类id, 填写后只生成某个分类下的接口，默认生成该项目所有接口请求代码 | 
| enableValidte | Boolean | true| 是否开启参数验证，默认是开启的，需要引入 ava 依赖|


### 自定义模板

自定义模板就是写一个函数，返回一个 request 函数片段，接收两个参数，第一个 baseinfo,包含了请求 url, method, 请求参数等

```js
{
  template: function(){
    return `function request(baseinfo, options = {}){
        let params = baseinfo.params;

        options = Object.assign({}, {
          url: baseinfo.url,
          method: baseinfo.method,
          data: params
        }, options)

        if(checkRequestParams && typeof checkRequestParams === 'function'){
          checkRequestParams(interfaceData ,params)
        }

        return axios(options)
      }`
  }
}


```



## Browsers support

Modern browsers and Internet Explorer 10+.

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="IE / Edge" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>IE / Edge | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Firefox | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Chrome | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_48x48.png" alt="Safari" width="24px" height="24px" />](http://godban.github.io/browsers-support-badges/)</br>Safari |
| --------- | --------- | --------- | --------- |
| IE10, IE11, Edge| last 2 versions| last 2 versions| last 2 versions

