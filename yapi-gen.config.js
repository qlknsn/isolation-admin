/*
 * @Author: your name
 * @Date: 2020-02-10 23:00:18
 * @LastEditTime : 2020-02-14 09:59:27
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \isolation-admin\yapi-gen.config.js
 */
const baseUrl = "/adminapi"
module.exports = {
    server: 'http://192.168.63.200:3000',
    // server: 'http://192.168.202.243:8088',
    token: '1fdbb020ab34d670f41d65945159b3c1ece0b4f3d15ab66ac07e27d5ac03e6d6',
    dist: "./src/api/gen-api.js",
    methodName: function(apipath, method) {
        let apipaths = apipath.split('/')
        apipaths.shift();
        let name = []
        name = [method.toLowerCase()]
        name = [].concat(name, apipaths)
        name = name.map(p => {
            return p.replace(/[^a-zA-Z0-9\_]+/g, '')
        })

        return name.join('_')
    },
    template: function() {
        return `function request(baseinfo, options = {}){
            let params = baseinfo.params;
            
            
            options = Object.assign({}, {
              url:"${baseUrl}"+baseinfo.url,
              method: baseinfo.method,
              data: params
            }, options)
  
    
            return axios(options)
          }`
    }
}